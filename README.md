Gone - A Game developed at the Global Game Jam 2014
===================================================

As Thomas' wife died in a car crash, he couldn't recover from the pain it inflicted. Thomas is blind and cannot
manage his everyday live by himself. To not leave Thomas alone in this terrible situation, his wife comes back as a
ghost and interacts with objects in their house to guide Thomas and provide emotional closure. Setup: Player 1 is using
a laptop and playes the ghost of Thomas' wife. She can interact with objects by turning on and off sound spots in a
ground plan of their house. Player 2 (Thomas) is sitting opposite the other player with headphones to hear the sound
becoming louder or quieter depending on his position and field of view. He can also hear the direction of the sound
and move forward or backward with a gamepad. The orientation is controlled by an Oculus RIFT which also is used as a
blindfold to simulate the blindness of Thomas. The goal of the game "Gone" is to guide Thomas to all relevant positions
in their house only by sound. When he reaches the spots, he remembers positive and negative events from their joined
live by hearing some music or sounds (e.g. the song of their wedding). After finding all the sound spots, Thomas finds
peace at last and can get some closure.

see also: http://globalgamejam.org/2014/games/gone

download a binary version for Linux, Win and Mac: http://www.itec.uni-klu.ac.at/~mlux/files/Gone-binary-2014-01-27.7z

Credits:
========

-  Mathias Lux (Game Design, Programming)
-  Michael Riegler (Game Design, Programming, Documentation)
-  Norbert Spot (Programming)
-  Manuel Zoderer (Programming)
-  Julian Kogler (Game Design)
-  Andreas Leibetseder (Programming)
-  Lukas Knoch (Programming)
-  Christian Zellot (Programming)
-  Horst Schnattler (Sound Design)
-  Claus Degendorfer (Management)
-  Gabriel Napetschnig (Graphical Design)


Game Media Licenses (Sounds & Icons)
====================================

-  Radio sound CC BY Timbre, freesound.org
-  Hair dryer sound CC BY organicmanpl, freesound.org
-  Alarm clock sound CC BY bone666138, freesound.org
-  Tap sound CC BY RutgerMuller, freesound.org
-  Fan sound CC BY ashleyxxpiano, freesound.org
-  Telephone sound CC BY winsx87, freesound.org
-  Icons CC BY by Joe Harrison from The Noun Project