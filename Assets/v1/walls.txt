# x;y;w;h
-- world
0;0;600;10		# bottom wall
0;0;10;600		# left wall
0;590;600;10	# top wall
590;0;10;600	# right wall
340;0;10;250	# vertical wall long 1
140;240;10;35	# vertical wall short 1
140;325;10;35	# vertical wall short 2
270;350;10;270	# vertical wall long 2
0;240;175;10	# horizontal wall top 1
226;240;174;10	# horizontal wall top 2
450;240;150;10	# horizontal wall top 3
0;350;175;10	# horizontal wall bot 1
226;350;174;10	# horizontal wall bot 2
449;350;150;10	# horizontal wall bot 3

-- objects
20;580;fan;10
290;400;fan;10
558;52;fan;10
300;508;hair_dryer;16
20;340;hair_dryer;16
295;140;tap;4
20;290;tap;4
100;285;radio;14
550;400;radio;14
20;80;radio;14
360;180;radio;14
513;580;alarm_clock;4
50;225;alarm_clock;4
300;290;clock;10
20;485;clock;10
550;220;clock;10
165;345;telephone;6
550;340;plant;1
226;405;plant;1
306;54;plant;1
550;500;plant;1
300;230;plant;1
220;580;telephone;6