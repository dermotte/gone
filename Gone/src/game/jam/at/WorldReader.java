package game.jam.at;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class WorldReader {
	private String filePath;
	private List<PolygonShape> rectangles;
	private List<GameObject> gameObjects;
	private boolean readFile;
	private float viewPortHeight;
	private float viewPortWidth;
	
	private final int ICON_SIZE = 32;
	
	
	public WorldReader(String filePath, float viewportHeight, float viewportWidth){
		this.filePath = filePath;
		this.rectangles = new ArrayList<PolygonShape>();
		this.gameObjects = new ArrayList<GameObject>();
		this.readFile = false;
		this.viewPortHeight = viewportHeight;
		this.viewPortWidth = viewportWidth;
	}
	
	public void readFile(){
		BufferedReader br = null;
		try {
		    
			br = new BufferedReader(new FileReader(filePath));
	        String line = br.readLine();
	        int section = -1; //section 0 walls, section 1 objects
	        while (line != null) {
	        	line = br.readLine();
	        	if(line == null){
	        		continue;
	        	}
	        	if(line.equals("#x;y;w;h")){
	        		continue;
	        	}else if(line.equals("-- world")){
	        		section = 0;
	        		continue;
	        	}else if(line.equals("-- objects")){
	        		section = 1;
	        		continue;
	        	}
	        	if(!line.equals("")){
		        	switch(section){
		        		case 0:
		        			parseWallLine(line);
		        			break;
		        		case 1:
		        			parseObjectLine(line);
		        			break;
		        	}
	        	}
	        }
	        
	        readFile = true;
	        
	    } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	    	if(br != null){
	    		try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	}
	    }
		
	}
	
	
	private void parseWallLine(String lineContent){
		if(lineContent != null){
			String[] contentComments = lineContent.split("#");
			if(contentComments.length < 1){
				throw new RuntimeException("invalid input in file"); 
			}
			String[] coords = contentComments[0].split(";"); //x;y;w;h
			PolygonShape groundBox = new PolygonShape();
			int x = Integer.parseInt(coords[0].replaceAll("\\s+",""));
			int y = Integer.parseInt(coords[1].replaceAll("\\s+",""))-10;
			int w = Integer.parseInt(coords[2].replaceAll("\\s+",""));
			int h = Integer.parseInt(coords[3].replaceAll("\\s+",""));
			groundBox.setAsBox(w/2, h/2, new Vector2((float)x+(w/2.f), (float)y+(h/2.f)), 0);
			System.out.println("w " + w + " h " + h + " x " + x + " y " + y);
			rectangles.add(groundBox);
		}
	}
	
	private void parseObjectLine(String lineContent){
		String[] row = lineContent.split(";");
		int x = Integer.parseInt(row[0]);
		int y = Integer.parseInt(row[1]);
		String iconName = row[2];
		String soundName = row[2];
		long soundLength = Long.parseLong(row[3]);
		System.out.println("object line x " +x + " y " + y + " icon " + iconName + " sound " + soundName);
		
		gameObjects.add(new GameObject(x, y, soundName, iconName, (soundLength)*1000, viewPortWidth, viewPortHeight));

	}
	
	
	
	public void setFilePath(String filePath){
		this.filePath = filePath;
	}
	
	public String getFilePath(){
		return this.filePath;
	}
	
	public List<PolygonShape> getRectangles(){
		return rectangles;
	}
	
	public void setRectangles(List<PolygonShape> rectangles){
		this.rectangles = rectangles;
	}
	
	public boolean fileWasRead(){
		return readFile;
	}
	
	public List<GameObject> getGameObjects(){
		return this.gameObjects;
	}
	
	public void setGameObjects(List<GameObject> gameObjects){
		this.gameObjects = gameObjects;
	}
	
	

}
