package game.jam.at;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import java.util.List;

public class MainGameScreen implements Screen, InputProcessor {
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private Texture texture;
    private Sprite sprite;
    Vector2 audioSource = new Vector2(600, 600);
    private WorldReader worldReader;
    private List<GameObject> gameObjects;
    private Sprite memorySprite, roseSprite;

    float aniMemory = 0f;

    private GoneGame game;

    private static final int ICON_SIZE_MEMORY = 64;
    /**
     * the renderer
     */
    protected Box2DDebugRenderer renderer;

    /**
     * our box2D world
     */
    protected World world;

    private Body ghostBody;
    private Vector2 direction;
    private float movement;
    private Music music = null;
    private Sprite s;
    private boolean playedMemory;


    public MainGameScreen(GoneGame Game) {
        game = Game;
    }

    public void create() {

        // load sounds
        // music = Gdx.audio.newMusic(Gdx.files.internal("assets/radio.wav"));
        // music.play();
        // music.setLooping(true);

        // maby no good idea?
        Texture.setEnforcePotImages(false);

        camera = new OrthographicCamera();
        camera.viewportHeight = 600;
        camera.viewportWidth = 800;
        camera.position.set(camera.viewportWidth * .5f,
                camera.viewportHeight * .5f, 0f);

        camera.update();
        batch = new SpriteBatch();
        // create the debug renderer
        renderer = new Box2DDebugRenderer();

        texture = new Texture(Gdx.files.internal("Assets/v1/house.png"));
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);


        Texture t1;
        t1 = new Texture(Gdx.files.internal("Assets/icons/man_32.png"));
        t1.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        Texture roseTexture = new Texture(Gdx.files.internal("Assets/rose_grow.png"));
        roseTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        roseSprite = new Sprite(GoneGame.model.getRoseTexture());
//        roseSprite.setSize(131,300);
        roseSprite.setOrigin(0, 0);

        s = new Sprite(new TextureRegion(t1, 0, 0, 32, 32));
        s.setSize(32f, 32f);
        s.setOrigin(0, 0);

        TextureRegion region = new TextureRegion(texture, 0, 0, 800, 600);

        sprite = new Sprite(region);
        sprite.setSize(camera.viewportHeight, camera.viewportWidth);
        sprite.setOrigin(0, 0);
        sprite.setPosition(-camera.viewportWidth * .5f,
                -camera.viewportHeight * .5f);
        Gdx.input.setInputProcessor(this);
        createWorld();

        // memory
        Texture mtexture = new Texture(
                Gdx.files.internal("Assets/icons/memory_blob.png"));
        mtexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        TextureRegion mregion = new TextureRegion(mtexture, 0, 0,
                ICON_SIZE_MEMORY, ICON_SIZE_MEMORY);
        memorySprite = new Sprite(mregion);
        memorySprite.setSize(ICON_SIZE_MEMORY, ICON_SIZE_MEMORY);
        memorySprite.setOrigin(0, 0);
        memorySprite.setPosition(GoneGame.model.getMemoryPosition().x, GoneGame.model.getMemoryPosition().y);
//		memorySprite.setPosition(-camera.viewportWidth * .5f + 100
//				- (ICON_SIZE_MEMORY / 4), -camera.viewportHeight * .5f + 550
//				- (ICON_SIZE_MEMORY));
    }

    private void createWorld() {

        world = new World(new Vector2(0, 0), true);
        generateAndPlaceGhost();

        // add Listener to controller if exists
        if (Controllers.getControllers().size != 0) {
            Controllers.getControllers().first()
                    .addListener(new ControllerAdapter(ghostBody, this));
        }

    }

    public float getMovement() {
        return movement;
    }

    public void setMovement(float movement) {
        this.movement = movement;
    }

    /**
     * generates the Gosts's body and places it
     */
    private void generateAndPlaceGhost() {
        // Ghost Body
        world = new World(new Vector2(0, 0), true);

        // Ground body
        BodyDef groundBodyDef = new BodyDef();
        groundBodyDef.position.set(new Vector2(0, 10));
        Body groundBody = world.createBody(groundBodyDef);
        worldReader = new WorldReader("Assets/v1/walls.txt",
                camera.viewportHeight, camera.viewportWidth);
        worldReader.readFile();
        for (PolygonShape shape : worldReader.getRectangles()) {
            groundBody.createFixture(shape, 0.0f);
        }
        gameObjects = worldReader.getGameObjects();
        // Dynamic Body
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        bodyDef.position.set(3 * camera.viewportWidth / 5,
                camera.viewportHeight / 4);
        ghostBody = world.createBody(bodyDef);
        CircleShape dynamicCircle = new CircleShape();
        dynamicCircle.setRadius(10f);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = dynamicCircle;
        fixtureDef.density = 0.5f;
        fixtureDef.friction = 0.0f;
        fixtureDef.restitution = 0;
        ghostBody.createFixture(fixtureDef);
        renderer = new Box2DDebugRenderer();
    }

    @Override
    public void dispose() {
        batch.dispose();
        texture.dispose();
        // TODO handle all disposes and co...
    }

    public void render(float delta) {

        // calculate aming direction of ghost
        direction = new Vector2(((float) Math.cos(ghostBody.getAngle()))
                * movement, ((float) (Math.sin(ghostBody.getAngle())))
                * movement);
        ghostBody.setLinearVelocity(direction);

        // s.setPosition(ghostBody.getPosition().x,ghostBody.getPosition().y);
        s.setPosition(-camera.viewportWidth * .5f + ghostBody.getPosition().x
                - (32 / 2f),
                -camera.viewportHeight * .5f + ghostBody.getPosition().y
                        - (32 / 2f));
        s.setRotation((float) Math.toDegrees(ghostBody.getAngle()));

//        roseSprite.setPosition(400, 50);
        aniMemory += Gdx.graphics.getDeltaTime();
        aniMemory = aniMemory - Math.round(aniMemory);

        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.projection);


        batch.begin();
        batch.draw(sprite, sprite.getX(), sprite.getY());
        // roseSprite.setColor(1,1,1,1);
        Color c = batch.getColor();
//        batch.setColor(c.r, c.g, c.b, .15f);//set alpha to 0.15
        if (GoneGame.model.getStage()>0) batch.draw(roseSprite, 170, -300);
        batch.setColor(c.r, c.g, c.b, aniMemory);//set alpha to animation value
        if (music == null) {
            batch.draw(memorySprite, memorySprite.getX(), memorySprite.getY());
        } else if (!music.isPlaying()) batch.draw(memorySprite, memorySprite.getX(), memorySprite.getY());

        batch.setColor(c.r, c.g, c.b, 1f); //set alpha to 1
        for (GameObject gameObject : gameObjects) {
            batch.draw(gameObject.refreshSprite(), gameObject.getSprite()
                    .getX(), gameObject.getSprite().getY());
            // System.out.println(music.getVolume());
            if (gameObject.isActivated()) {
                Vector2 as = new Vector2(gameObject.getX(), gameObject.getY());
                float distance = getDistance(ghostBody.getPosition(), as);
                // System.out.println("distance"+distance);
                float volume = (float) Math.log(Math.E - (distance / 800f)
                        * Math.E);
                // System.out.println(ghostBody.getAngle() % (2 * Math.PI));
                float angle = calculateAngle(as);
                float pan = (float) Math.sin(angle);
                // System.out.println("pan" + pan);

                angle = (float) Math.toDegrees(angle);
                while (angle <= 0) {
                    angle += 360;
                }
                while (angle > 360) {
                    angle -= 360;
                }
                System.out.println(angle);

                boolean back = angle < 90
                        || angle > 265;
                gameObject.setSoundPan(pan, volume, !back);
                // gameObject.getMusic().play();
            }
        }

        // System.out.println("x " + ghostBody.getPosition().x);
        // System.out.println("y " + ghostBody.getPosition().y);


        // This is where the memory is found ... TODO: Extend to four memories..
//        System.out.println(ghostBody.getPosition().dst(GoneGame.model.getMemoryPosition()) + " " + ghostBody.getPosition().toString() + " " + GoneGame.model.getMemoryPosition().toString());
        if (GoneGame.model.getDistance(ghostBody.getPosition()) < 40) {
//		if ((100 < ghostBody.getPosition().x) && (140 > ghostBody.getPosition().x) && (500 < ghostBody.getPosition().y) && (540 > ghostBody.getPosition().y) && !playedMemory) {
            music = GoneGame.model.getMusic();//Gdx.audio.newMusic(Gdx.files.internal("Assets/sfx/voice_grammophon_mit_sfx.wav"));
//            music = Gdx.audio.newMusic(Gdx.files.internal("Assets/sfx/voice_grammophon_mit_sfx.wav"));
            music.play();
            music.setLooping(false);
//			memorySprite.setPosition(-1000, -1000); // out of sight out of mind
            GoneGame.model.nextStage();
            memorySprite.setPosition(GoneGame.model.getMemoryPosition().x, GoneGame.model.getMemoryPosition().y);
            roseSprite.setTexture(GoneGame.model.getRoseTexture());
        }
        batch.draw(s, s.getX(), s.getY(), 16, 16, 32, 32, 1, 1,
                (float) (Math.toDegrees(ghostBody.getAngle()) - 90));
        batch.end();

        // renderer.render(world, camera.combined);

        // set fps
        world.step(1 / 60f, 6, 2);

        if (GoneGame.model.isFinished()) {
            if (!music.isPlaying()) {
                game.setCredits();
            }
        }

    }

    /**
     * @param audioSource
     * @return the angle in radian between the player and the audioSource
     */
    private float calculateAngle(Vector2 audioSource) {
        Vector2 v1 = audioSource.sub(ghostBody.getPosition());

        return (float) Math.atan2(v1.y, v1.x) - ghostBody.getAngle();

    }

    private float getDistance(Vector2 posA, Vector2 posB) {
        return (float) Math.sqrt(Math.pow((posB.x - posA.x), 2)
                + Math.pow((posB.y - posA.y), 2));

    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == 22) { // rechts
            ghostBody.setAngularVelocity(-5);
        } else if (keycode == 21) { // links
            ghostBody.setAngularVelocity(5);
        } else if (keycode == 20) { // down
            this.setMovement(-50);
        } else if (keycode == 19) { // up
            this.setMovement(50);
        }
        // rechts 22
        // links 21
        // owe 20
        // auffe 19
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == 22) { // rechts
            ghostBody.setAngularVelocity(0);
        } else if (keycode == 21) { // links
            ghostBody.setAngularVelocity(0);
        } else if (keycode == 20) { // down
            this.setMovement(0);
        } else if (keycode == 19) { // up
            this.setMovement(0);
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        // System.out.println("touchDown: screenX " + screenX + " screenY " +
        // screenY + " pointer " + pointer + " button " + button);
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        // System.out.println("touchUp: screenX " + screenX + " screenY "
        // + screenY + " pointer " + pointer + " button " + button);
        for (GameObject go : gameObjects) {
            go.changeIfClickInRange(screenX, screenY);
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        // System.out.println("mouseMoved: screenX " + screenX + " screenY " +
        // screenY);
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void show() {
        create();

    }

    @Override
    public void hide() {

    }

}
