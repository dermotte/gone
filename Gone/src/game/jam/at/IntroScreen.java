package game.jam.at;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;

public class IntroScreen implements Screen, InputProcessor {
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private Sprite sprite;

    private Texture firstShot;
    private Texture secondShot;
    private Texture thirdShot;

    private Music crash;
    private Music funeral;
    private Music ghostappear;

    private GoneGame game;

    float spriteX;
    float rotation = 6f;
    float scale = 1f;

    float rotationOffset = 0.5f, scaleOffset = 0.05f;

    float time2displayhelp = 10f;

    int screenID;

    boolean loadingFinished = false;
    /**
     * the renderer
     */
    protected Box2DDebugRenderer renderer;

    public IntroScreen(GoneGame Game) {
        game = Game;
    }

    public void create() {

        spriteX = -390.0f;
        screenID = 0;

        Texture.setEnforcePotImages(false);
        // create the camera and the SpriteBatch
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 600);
        batch = new SpriteBatch();

        // create the debug renderer
        renderer = new Box2DDebugRenderer();

        drawTexture("Assets/v1/firstShot.png", "Assets/sounds/panel1_crash.wav");
//		"Assets/sounds/panel1_crash.wav"
//		"Assets/v1/firstShot.png"
        Gdx.input.setInputProcessor(this);
    }

    public void drawTexture(String picPath, String soundPath) {
        if (soundPath != null) {
            crash = Gdx.audio.newMusic(Gdx.files.internal(soundPath));
            crash.setLooping(false);
            crash.play();
        }

        firstShot = new Texture(Gdx.files.internal(picPath));
        TextureRegion region = new TextureRegion(firstShot, 0, 0, 597, 600);
        Texture texture = region.getTexture();
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        sprite = new Sprite(texture);
        sprite.setSize(camera.viewportHeight, camera.viewportWidth);
        sprite.setOrigin(0, 0);
        spriteX = -camera.viewportWidth / 3;
        sprite.setPosition(spriteX, -camera.viewportHeight / 3);
//		sprite.setRotation(25);
        screenID++;
    }

    @Override
    public void dispose() {

    }

    @Override
    public void render(float f) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        // check if it is loaded or not.
        if (!loadingFinished) {
            loadingFinished = GoneGame.assMan.update();
        }

        batch.setProjectionMatrix(camera.projection);

//		spriteX += Gdx.graphics.getDeltaTime() * 0.5f;
        rotation += Gdx.graphics.getDeltaTime() * rotationOffset;
        scale += Gdx.graphics.getDeltaTime() * scaleOffset;

        float sw = 2f * camera.viewportWidth / 3f;
        float sh = 2f * camera.viewportHeight / 3f;
        batch.begin();
        // batch.setTransformMatrix(new Matrix4());
        batch.draw(sprite, spriteX, sprite.getY(), sw / 2, sh / 2, sw, sh, scale, scale, rotation);
        batch.end();

        switch (screenID) {
            case 1: {
                if (!crash.isPlaying()) {
                    System.out.println("has stopped");
                    drawTexture("Assets/v1/secondShot.png", "Assets/sounds/panel2_funeral.wav");
                    scale = 1f;
                    rotation = -3;
                    rotationOffset = 0.05f;
                    scaleOffset = 0.03f;
                }
            }
            break;

            case 2: {
                if (!crash.isPlaying()) {
                    System.out.println("has stopped");
                    drawTexture("Assets/v1/thirdShot.png", "Assets/sounds/panel3_ghostappear.wav");
                    scale = 1f;
                    rotation = 0;
                    rotationOffset = 0.0f;
                    scaleOffset = 0.05f;

                }
            }
            break;

            case 3:
                if (!crash.isPlaying()) {
                    System.out.println("has stopped");
                    drawTexture("Assets/v1/help.png", null);
                    scale = 1f;
                    rotation = 0;
                    rotationOffset = 0.0f;
                    scaleOffset = 0.00f;
                    if (time2displayhelp < 0) game.setMenu();
                }
                break;

            case 4:
                time2displayhelp -= Gdx.graphics.getDeltaTime();
                if (time2displayhelp < 0) game.setMenu();
                break;

            default:
                break;
        }


//		sprite.draw(batch);
//		for(GameObject gameObject : gameObjects){
//			batch.draw(gameObject.getSprite(), gameObject.getSprite().getX(), gameObject.getSprite().getY());
//		}


//		sprite.setX(2 * Gdx.graphics.getDeltaTime());

//		renderer.render(world, camera.combined);

        // set fps
//		world.step(1 / 60f, 6, 2);
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void show() {
        create();
    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }


    @Override
    public boolean keyDown(int i) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean keyUp(int i) {
        if (i == Input.Keys.ESCAPE) { // up
            crash.stop();
            crash.dispose();
            GoneGame.assMan.finishLoading();
            game.setMenu();
        }

        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean keyTyped(char c) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean touchDown(int i, int i2, int i3, int i4) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean touchUp(int i, int i2, int i3, int i4) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean touchDragged(int i, int i2, int i3) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean mouseMoved(int i, int i2) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean scrolled(int i) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}