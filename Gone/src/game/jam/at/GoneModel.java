package game.jam.at;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

import javax.xml.soap.Text;

/**
 * Created by mlux on 21.05.14.
 */
public class GoneModel {
    private int stage;
    private boolean finished = false;
    Vector2 memoryPosition = new Vector2(-310, 150);

    public void init() {
        stage = 0;
        finished = false;
        memoryPosition.set(-310, 150);
    }

    public int getStage() {
        return stage;
    }

    public Texture getRoseTexture() {
        Texture rose = null;
        switch (stage) {
            case 0:
//                rose = GoneGame.assMan.get("Assets/black_32.png", Texture.class);
                rose = GoneGame.assMan.get("Assets/rose/rose_3.png", Texture.class);
                break;
            case 1:
                rose = GoneGame.assMan.get("Assets/rose/rose_0.png", Texture.class);
                break;
            case 2:
                rose = GoneGame.assMan.get("Assets/rose/rose_1.png", Texture.class);
                break;
            case 3:
                rose = GoneGame.assMan.get("Assets/rose/rose_2.png", Texture.class);
                break;
            case 4:
                rose = GoneGame.assMan.get("Assets/rose/rose_3.png", Texture.class);
                finished = true;
                break;
            default:
                return null;
        }
        return rose;
    }

    public boolean nextStage() {
        stage++;
        switch (stage) {
            case 0:
                memoryPosition.set(-310, 150);
                break;
            case 1:
                memoryPosition.set(35, 150);
                break;
            case 2:
                memoryPosition.set(-310, -150);
                break;
            case 3:
                memoryPosition.set(35, -150);
                break;
            case 4:
                memoryPosition.set(-1000, -1000);
                finished = true;
                break;
            default:
                return false;
        }
        return true;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public boolean isFinished() {
        return finished;
    }

    public Vector2 getMemoryPosition() {
        return memoryPosition;
    }

    public float getDistance(Vector2 ghost) {
        return ghost.dst(memoryPosition.x + 432, memoryPosition.y + 332);
    }

    public Music getMusic() {
        Music music = null;
        switch (stage) {
            case 0:
                music = GoneGame.assMan.get("Assets/sfx/voice_klospuelung.wav", Music.class);
                break;
            case 1:
                music = GoneGame.assMan.get("Assets/sfx/voice_dice_mit_sfx.wav", Music.class);
                break;
            case 2:
                music = GoneGame.assMan.get("Assets/sfx/voice_glass_mit_sfx.wav", Music.class);
                break;
            case 3:
                music = GoneGame.assMan.get("Assets/sfx/voice_grammophon_mit_sfx.wav", Music.class);
                break;
            default:
                return null;
        }
        return music;
    }
}
