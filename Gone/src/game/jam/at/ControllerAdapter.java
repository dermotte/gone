package game.jam.at;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;

public class ControllerAdapter implements ControllerListener {

	private Body ghostBody;
	private float movement;
	private MainGameScreen game;

	public ControllerAdapter(Body b, MainGameScreen g) {
		this.ghostBody = b;
		this.game = g;
	}

	@Override
	public boolean accelerometerMoved(Controller arg0, int arg1, Vector3 arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean axisMoved(Controller controller, int axisCode, float value) {

		switch (axisCode) {
		case 1:
			movement = (float) (Math.round(value * 100.0) / 100.0) * -1000;

			break;
		case 2:
			ghostBody.setAngularVelocity(-value * 5);

			break;
		default:
			return false;

		}
		System.out.println("x=" +ghostBody.getPosition().x);
		System.out.println("y=" +ghostBody.getPosition().y);
		game.setMovement(movement);
		return true;
	}

	@Override
	public boolean buttonDown(Controller arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean buttonUp(Controller arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void connected(Controller arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void disconnected(Controller arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean povMoved(Controller arg0, int arg1, PovDirection arg2) {

		return false;
	}

	@Override
	public boolean xSliderMoved(Controller arg0, int arg1, boolean arg2) {

		return false;
	}

	@Override
	public boolean ySliderMoved(Controller arg0, int arg1, boolean arg2) {
		return false;
	}

}
