package game.jam.at;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by mlux on 21.05.14.
 */
public class HowToScreen implements Screen {
    private GoneGame game;
    BitmapFont font = null;
    Texture bgImage = null;
    private OrthographicCamera camera;
    private SpriteBatch batch;
    float h,l;

    public HowToScreen(GoneGame game) {
        this.game = game;
    }

    @Override
    public void render(float delta) {
        font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        h = 600;
        l = font.getLineHeight() + 18;


        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);


        if (Gdx.input.justTouched()) {
            game.setMenu();
        }

        batch.begin();
        batch.draw(bgImage, 0, 0);
//        font.draw(batch, "Start Gone ...", l, h - l);

        batch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {
        bgImage = GoneGame.assMan.get("Assets/v1/help.png", Texture.class);
        font = GoneGame.assMan.get("Assets/menufont.fnt", BitmapFont.class);
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 600);
//        camera.position.set(0, 0, 0);
        batch = new SpriteBatch();

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
