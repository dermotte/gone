package game.jam.at;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class GoneGame extends Game {
    public static AssetManager assMan = new AssetManager();
    public static GoneModel model = new GoneModel();
    MainGameScreen mainGame;
    IntroScreen intro;
    CreditsScreen creditsScreen;
    MenuScreen menu;
    private Screen howTo;


    @Override
    public void create() {
        // loading stuff here ...
        assMan.load("Assets/menufont.fnt", BitmapFont.class);
        assMan.load("Assets/menu_background.png", Texture.class);
        assMan.load("Assets/v1/help.png", Texture.class);
        assMan.load("Assets/rose/rose_0.png", Texture.class);
        assMan.load("Assets/rose/rose_1.png", Texture.class);
        assMan.load("Assets/rose/rose_2.png", Texture.class);
        assMan.load("Assets/rose/rose_3.png", Texture.class);

        assMan.load("Assets/icons/alarm_clock_32.png", Texture.class);
        assMan.load("Assets/icons/alarm_clock_h_32.png", Texture.class);
        assMan.load("Assets/icons/clock_32.png", Texture.class);
        assMan.load("Assets/icons/clock_h_32.png", Texture.class);
        assMan.load("Assets/icons/fan_32.png", Texture.class);
        assMan.load("Assets/icons/fan_h_32.png", Texture.class);
        assMan.load("Assets/icons/hair_dryer_32.png", Texture.class);
        assMan.load("Assets/icons/hair_dryer_h_32.png", Texture.class);
        assMan.load("Assets/icons/man_32.png", Texture.class);
        assMan.load("Assets/icons/memory_blob.png", Texture.class);
        assMan.load("Assets/icons/plant_32.png", Texture.class);
        assMan.load("Assets/icons/plant_h_32.png", Texture.class);
        assMan.load("Assets/icons/radio_32.png", Texture.class);
        assMan.load("Assets/icons/radio_h_32.png", Texture.class);
        assMan.load("Assets/icons/tap_32.png", Texture.class);
        assMan.load("Assets/icons/tap_h_32.png", Texture.class);
        assMan.load("Assets/icons/telephone_32.png", Texture.class);
        assMan.load("Assets/icons/telephone_h_32.png", Texture.class);
        assMan.load("Assets/sfx/alarm_clock.wav", Sound.class);

        assMan.load("Assets/sfx/alarm_clock_back.wav", Sound.class);
        assMan.load("Assets/sfx/clock.wav", Sound.class);
        assMan.load("Assets/sfx/clock_back.wav", Sound.class);
        assMan.load("Assets/sfx/fan.wav", Sound.class);
        assMan.load("Assets/sfx/fan_back.wav", Sound.class);
        assMan.load("Assets/sfx/hair_dryer.wav", Sound.class);
        assMan.load("Assets/sfx/hair_dryer_back.wav", Sound.class);
        assMan.load("Assets/sfx/plant.wav", Sound.class);
        assMan.load("Assets/sfx/plant_back.wav", Sound.class);
        assMan.load("Assets/sfx/radio.wav", Sound.class);
        assMan.load("Assets/sfx/radio_back.wav", Sound.class);
        assMan.load("Assets/sfx/tap.wav", Sound.class);
        assMan.load("Assets/sfx/tap_back.wav", Sound.class);
        assMan.load("Assets/sfx/telephone.wav", Sound.class);
        assMan.load("Assets/sfx/telephone_back.wav", Sound.class);
        assMan.load("Assets/sfx/voice_dice_mit_sfx.wav", Music.class);
        assMan.load("Assets/sfx/voice_glass_mit_sfx.wav", Music.class);
        assMan.load("Assets/sfx/voice_grammophon_mit_sfx.wav", Music.class);
        assMan.load("Assets/sfx/voice_klospuelung.wav", Music.class);

        assMan.update();
        //
        intro = new IntroScreen(this);
        mainGame = new MainGameScreen(this);
        creditsScreen = new CreditsScreen(this);
        menu = new MenuScreen(this);
        howTo = new HowToScreen(this);
        setIntro();
    }

    public void setIntro() {
        intro = new IntroScreen(this);
        setScreen(intro);
    }

    public void setGame() {
        model.init();
        mainGame = new MainGameScreen(this);
        setScreen(mainGame);
    }

    public void setCredits() {
        setScreen(creditsScreen);
    }

    public void setMenu() {
        setScreen(menu);
    }

    public void setHowTo() {
        setScreen(howTo);
    }
}