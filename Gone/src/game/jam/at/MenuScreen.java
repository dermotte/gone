package game.jam.at;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by mlux on 21.05.14.
 */
public class MenuScreen implements Screen {
    private GoneGame game;
    BitmapFont font = null;
    Texture bgImage = null;
    private OrthographicCamera camera;
    private SpriteBatch batch;
    float h,l;

    public MenuScreen(GoneGame goneGame) {
        game = goneGame;
    }

    @Override
    public void show() {

        bgImage = GoneGame.assMan.get("Assets/menu_background.png", Texture.class);
        font = GoneGame.assMan.get("Assets/menufont.fnt", BitmapFont.class);
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 600);
//        camera.position.set(0, 0, 0);
        batch = new SpriteBatch();
    }

    @Override
    public void render(float delta) {

        font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        h = 600;
        l = font.getLineHeight() + 18;


        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);


        if (Gdx.input.justTouched()) {
            // (x,y) from top left corner
            float x = Gdx.input.getX()/ ((float) Gdx.graphics.getWidth())* ((float) 800);
            float y = Gdx.input.getY()/ ((float) Gdx.graphics.getHeight())* ((float) 600);
            if (x > l && x < 2 * 800 / 3) {
                if (y > l + 5 && y < 2 * l - 10) { // new Game
                    game.setGame();
                } else if (y > 2 * l + 5 && y < 3 * l - 10) { // howto
                    game.setHowTo();
                } else if (y > 3 * l + 5 && y < 4 * l - 10) { // story
                    game.setIntro();
                } else if (y > 4 * l + 5 && y < 5 * l - 10) { // credits
                    game.setCredits();
                } else if (y > 5 * l + 5 && y < 6 * l - 10) { // not used.
//                    assets.options.gameState = GameOptions.STATE_CREDITS;
                }
            }
        }

        batch.begin();
        batch.draw(bgImage, 0, 0);
//        font.draw(batch, "Gone - The Game", 22, 100);

        font.draw(batch, "Start Gone ...", l, h - l);
        font.draw(batch, "How to interact?", l, h - 2 * l);
//        font.draw(batch, assets.options.getNumberOfCards() + "", 2 * MemoryGame.WIDTH / 3 - assets.font.getBounds(assets.options.getNumberOfCards() + "").width, h - 2 * l);
        font.draw(batch, "Show the story", l, h - 3 * l);
//        font.draw(batch, assets.options.getGameModeString(), 2 * MemoryGame.WIDTH / 3 - assets.font.getBounds(assets.options.getGameModeString()).width, h - 3 * l);
        font.draw(batch, "Credits", l, h - 4 * l);
//        font.draw(batch, (assets.options.soundOn)?"On":"Off", 2 * MemoryGame.WIDTH / 3 - assets.font.getBounds((assets.options.soundOn)?"On":"Off").width, h - 4 * l);
//        font.draw(batch, "> Credits", l, h - 5 * l);

        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
