package game.jam.at;

import java.util.Calendar;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class GameObject {
    private float x;
    private float y;
    private float viewPortWidth;
    private float viewPortHeight;
    private String soundName;
    private String iconName;
    private Sprite sprite;
    private boolean active;
    private Sound sound;
    private Sound soundBack;
    private long soundId;
    private long soundIdBack;
    private long soundStart;
    private long soundTime;

    private static final String iconPath = "Assets/icons/";
    private static final String soundPath = "Assets/sfx/";
    private static final int ICON_SIZE = 32;

    public GameObject(float x, float y, String soundName, String iconName,
                      long soundTime, float viewPortWidth, float viewPortHeight) {
        this.x = x;
        this.y = y;
        this.soundName = soundName;
        this.iconName = iconName;
        this.viewPortHeight = viewPortHeight;
        this.viewPortWidth = viewPortWidth;
        this.soundTime = soundTime;
        this.soundStart = -1;
        generateSprite();
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setSoundName(String soundName) {
        this.soundName = soundName;
    }

    public String getSoundName() {
        return this.soundName;
    }

    public String getSoundPath() {
        return soundPath + soundName + ".wav";
    }

    public String getSoundPathBack() {
        return soundPath + soundName + "_back.wav";
    }

    public String getIconName() {
        return this.iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getIconPath() {
        return iconPath + iconName + (active ? "_h_32.png" : "_32.png");
    }

    public Sprite getSprite() {
        return sprite;
    }


    public Sprite refreshSprite() {
        if (soundStart != -1 && (soundStart + soundTime < Calendar.getInstance().getTimeInMillis())) {
            System.out.println("deactivate sprite");
            active = !active;
            soundStart = -1;
            generateSprite();
        }
        return sprite;
    }

    public Sprite generateAndGetSprite() {

        generateSprite();
        return sprite;
    }

    public boolean changeIfClickInRange(int screenX, int screenY) {

        screenY = 600 - screenY;
        if (screenX > x - 20 && screenX < x + 20 && screenY > y - 20
                && screenY < y + 20) {

            if (!active) {
                active = true;
                activateMusic();

            }

            generateSprite();
            return true;
        }
        return false;
    }

    public void activateMusic() {
        sound = GoneGame.assMan.get(getSoundPath(), Sound.class);
        soundBack = GoneGame.assMan.get(getSoundPathBack(), Sound.class);
        soundId = sound.play();
        soundIdBack = soundBack.play();
        soundStart = Calendar.getInstance().getTimeInMillis();
    }


    public void deactivateMusic() {
        if (sound != null) {
            sound.stop();
            sound.dispose();
        }
        if (soundBack != null) {
            soundBack.stop();
            soundBack.dispose();
        }
    }

    public boolean isActivated() {

        return active;
    }

    public void setSoundPan(float pan, float volume, boolean back) {
        pan = -pan; //inverse!!
        if (sound != null && soundBack != null) {
            float cos = (float) Math.cos(Math.asin(pan));
            System.out.println(cos);
            if (!back) {
                sound.setPan(soundId, pan, volume);
                soundBack.setPan(soundIdBack, pan, 0);
            } else {
                sound.setPan(soundId, pan, 0);
                soundBack.setPan(soundIdBack, pan, volume);
            }
        }
    }

    private void generateSprite() {
        Texture texture = GoneGame.assMan.get(getIconPath(), Texture.class);
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
//        TextureRegion region = new TextureRegion(texture, 0, 0, ICON_SIZE,
//                ICON_SIZE);
        sprite = new Sprite(texture);
        sprite.setSize(ICON_SIZE, ICON_SIZE);
        sprite.setOrigin(0, 0);
        sprite.setPosition(-viewPortWidth * .5f + x - (ICON_SIZE / 4),
                -viewPortHeight * .5f + y - (ICON_SIZE));
    }

    public long soundTime() {
        return soundTime;
    }
}
