package game.jam.at;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;

public class CreditsScreen implements Screen {
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Sprite sprite;
    
    private Texture firstShot;

	private Music crash;

	private GoneGame game;
	
	float spriteY;
	
	int screenID;
	/**
	 * the renderer
	 */
	protected Box2DDebugRenderer renderer;
    private float minTime;

    public CreditsScreen(GoneGame Game) {
		game = Game;
	}

	public void create() {  
    	
    	spriteY = -1800.0f;
    	screenID = 0;
    	
    	Texture.setEnforcePotImages(false);
    	// create the camera and the SpriteBatch
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 600);
        batch = new SpriteBatch();
        
		// create the debug renderer
		renderer = new Box2DDebugRenderer();
    	
//		drawTexture("Assets/v1/GameOver.png", "Assets/sounds/musik_fuer_credits.wav");
		drawTexture("Assets/v1/credits.png", "Assets/sounds/musik_fuer_credits.wav");
//		"Assets/sounds/panel1_crash.wav"
//		"Assets/v1/firstShot.png"
    }
    
    public void drawTexture(String picPath, String soundPath) {
    	crash = Gdx.audio.newMusic(Gdx.files.internal(soundPath));
		crash.setLooping(false);
		crash.play();

    	firstShot = new Texture(Gdx.files.internal(picPath));
		TextureRegion region = new TextureRegion(firstShot, 0, 0, 800, 1600);
        Texture texture = region.getTexture();
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        sprite = new Sprite(texture);
		sprite.setSize(camera.viewportHeight, camera.viewportWidth);
		sprite.setOrigin(0, 0);
		sprite.setPosition(-400, spriteY);
//		sprite.setRotation(-25);
		screenID++;
    }

    @Override
    public void dispose() {
    
    }

    @Override
    public void render(float f) {
        minTime -= f;
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		batch.setProjectionMatrix(camera.projection);
		
		spriteY += Gdx.graphics.getDeltaTime() * 30.0f;
		
		batch.begin();
		batch.draw(sprite, sprite.getX(), spriteY);
		batch.end();
				
//		sprite.setX(2 * Gdx.graphics.getDeltaTime());

//		renderer.render(world, camera.combined);

		// set fps
//		world.step(1 / 60f, 6, 2);
        if (Gdx.input.isTouched() && minTime <=0) {
            crash.stop();
            game.setMenu();
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

	@Override
    public void show() {
        minTime = 2f;
		create();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}
}